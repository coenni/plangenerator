package com.lendico.finance.processor;

import com.lendico.finance.dto.PlanGenerateInputBean;

import com.lendico.finance.entity.MonthlyPaymentPlan;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import static com.lendico.finance.util.CONSTANTS.DAY_IN_MONTH;
import static com.lendico.finance.util.CONSTANTS.DAY_IN_YEAR;

public class PlanGenerator {

    LocalDateTime startDate;
    BigDecimal loanAmount;
    BigDecimal nominalInterestRate;
    int monthsInDuration;
    BigDecimal annuity;
    
    public PlanGenerator(LocalDateTime startDate, BigDecimal loanAmount, BigDecimal nominalInterestRate, int monthsInDuration) {
        this.startDate = startDate;
        this.loanAmount = loanAmount;
        this.nominalInterestRate = nominalInterestRate;
        this.monthsInDuration = monthsInDuration;

        calculateAnnuity();
    }

    public PlanGenerator( PlanGenerateInputBean planGenerateInputBean) {
        this.startDate = planGenerateInputBean.getStartDate();
        this.loanAmount = planGenerateInputBean.getLoanAmount();
        this.nominalInterestRate = planGenerateInputBean.getNominalRate().setScale(2);
        this.monthsInDuration = planGenerateInputBean.getDuration();
        calculateAnnuity();
    }

    private void calculateAnnuity() {
        BigDecimal interestMonthly =BigDecimal.valueOf(nominalInterestRate.doubleValue()/100/12);

        this.annuity = loanAmount.multiply(interestMonthly).multiply(BigDecimal.valueOf(Math.pow(interestMonthly.add(BigDecimal.ONE).doubleValue(), monthsInDuration))
        ).divide(
            (BigDecimal.valueOf(Math.pow(interestMonthly.add(BigDecimal.ONE).doubleValue(), monthsInDuration)).subtract(BigDecimal.ONE))
            , BigDecimal.ROUND_HALF_UP
        ).setScale(2, BigDecimal.ROUND_HALF_UP);
        System.out.println("annuity:" + annuity);
    }


    public List<MonthlyPaymentPlan> calculateMonthlyPaymentPlans() {
        List<MonthlyPaymentPlan> result = new ArrayList<>();
        BigDecimal initialOutstandingPrincipal = this.loanAmount;
        for (int i = 0; i < monthsInDuration; i++) {

            BigDecimal interest = nominalInterestRate.multiply(BigDecimal.valueOf(DAY_IN_MONTH))
                .multiply(initialOutstandingPrincipal )
                .divide(BigDecimal.valueOf(DAY_IN_YEAR*100), RoundingMode.HALF_UP)
                .setScale(2, BigDecimal.ROUND_HALF_UP);
            BigDecimal principal = this.annuity.subtract(interest);
            if(principal.compareTo(initialOutstandingPrincipal) ==1){
                principal = initialOutstandingPrincipal;
            }
            annuity = principal.add(interest);

            MonthlyPaymentPlan monthlyPaymentPlan = new MonthlyPaymentPlan(
                startDate, annuity, principal, interest, initialOutstandingPrincipal, initialOutstandingPrincipal.subtract(principal)
            );
            result.add(monthlyPaymentPlan);
            initialOutstandingPrincipal = initialOutstandingPrincipal.subtract(principal);
            startDate = startDate.plusMonths(1);
        }
        return result;
    }
}
