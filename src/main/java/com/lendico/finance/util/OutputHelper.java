package com.lendico.finance.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.lendico.finance.entity.MonthlyPaymentPlan;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class OutputHelper {

    public static void printToConsole(List<MonthlyPaymentPlan> monthlyPaymentPlanList) {
        DecimalFormat f = new DecimalFormat("#0.00");
        for (MonthlyPaymentPlan monthlyPaymentPlan:
            monthlyPaymentPlanList) {
            System.out.println(monthlyPaymentPlan.getDate() + "\t" +
                f.format(monthlyPaymentPlan.getAnnuity())+ "\t" +
                f.format(monthlyPaymentPlan.getPrincipal())+ "\t" +
                f.format(monthlyPaymentPlan.getInterest())  + "\t" +
                f.format(monthlyPaymentPlan.getInitialOutstandingPrincipal()) + "\t" +
                f.format(monthlyPaymentPlan.getRemainingOutstandingPrincipal()) + "\t"
            );
        }
    }

    public static void printJsonStringToConsole(List<MonthlyPaymentPlan> monthlyPaymentPlanList) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        String jsonResult = mapper.writeValueAsString(monthlyPaymentPlanList);
        System.out.println(jsonResult);
    }

    public static void printXmlStringToConsole(List<MonthlyPaymentPlan> monthlyPaymentPlanList) throws JsonProcessingException {

        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.registerModule(new JavaTimeModule());
        String xmlResult = xmlMapper.writeValueAsString(monthlyPaymentPlanList);
        System.out.println(xmlResult);

    }
}
