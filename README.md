#Plan Generator

*Swagger for EndPoint/Model/Dto Documentation*

[Swagger link](http://localhost:8080/swagger-ui.html)
http://localhost:8080/swagger-ui.html

Sample Queries, also Postman with request body JSON(application/json) for POST queries
```curl --header "Content-Type: application/json"  --request POST  --data '{ "loanAmount": "5000", "nominalRate": "5.0", "duration": 24, "startDate": "2018-01-01T00:00:01Z" }'  http://localhost:8080/generate-plan```



#Instructions
- For building:
```mvn clean package``` || ```mvnw clean package``` (can be with ```-DskipTests```)

    Expected Output: "...Building jar: \PlanGenerator\target\PlanGenerator-1.0-SNAPSHOT.jar ..."
- For Running: 
```java -jar target/PlanGenerator-1.0-SNAPSHOT.jar```