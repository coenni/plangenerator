package com.lendico.finance.controller;

import com.lendico.finance.dto.PlanGenerateInputBean;
import com.lendico.finance.entity.MonthlyPaymentPlan;
import com.lendico.finance.processor.PlanGenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.validation.Valid;

/**
 * REST controller for plan data.
 */
@org.springframework.web.bind.annotation.RestController
@RequestMapping
public class RestController {

    private final Logger log = LoggerFactory.getLogger(RestController.class);

    @PostMapping("/generate-plan")
    public ResponseEntity createCustomer(@Valid @RequestBody PlanGenerateInputBean planGenerateInputBean) throws URISyntaxException {
        log.info( "REST request to generate-plan with planGenerateInputBean : {}", planGenerateInputBean);
        List<MonthlyPaymentPlan> result = new PlanGenerator(planGenerateInputBean).calculateMonthlyPaymentPlans();
        return ResponseEntity.created(new URI("/generate-plan"))
            .body(result);
    }
}
