import com.fasterxml.jackson.core.JsonProcessingException;
import com.lendico.finance.entity.MonthlyPaymentPlan;
import com.lendico.finance.processor.PlanGenerator;
import com.lendico.finance.util.OutputHelper;

import org.junit.Test;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PlanGeneratorTest {


    @Test
    public void planGenerateTest() throws JsonProcessingException {

        BigDecimal loanAmount = BigDecimal.valueOf(5000.0);
        BigDecimal nominalInterestRate = BigDecimal.valueOf(5.0);
        int monthsInDuration = 2*12;
        DateTimeFormatter formatter =  DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");
        LocalDateTime startDate =   LocalDateTime.parse("2018-01-01T00:00:01Z", formatter);
        PlanGenerator planGenerator = new PlanGenerator(startDate, loanAmount, nominalInterestRate, monthsInDuration);
        List<MonthlyPaymentPlan> monthlyPaymentPlanList = planGenerator.calculateMonthlyPaymentPlans();

        OutputHelper.printToConsole(monthlyPaymentPlanList);
        OutputHelper.printJsonStringToConsole(monthlyPaymentPlanList);
        OutputHelper.printXmlStringToConsole(monthlyPaymentPlanList);

    }
}
