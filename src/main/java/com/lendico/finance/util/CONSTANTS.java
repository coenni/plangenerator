package com.lendico.finance.util;

public class CONSTANTS {
    public final static int DAY_IN_MONTH = 30;
    public final static int DAY_IN_YEAR = 360;
}
